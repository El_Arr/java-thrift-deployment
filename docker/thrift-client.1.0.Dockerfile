# syntax=docker/dockerfile:1
FROM openjdk:16-alpine3.13
WORKDIR /thrift
ADD "target/thrift-client-1.0.jar" ./
EXPOSE 9090
CMD java -jar thrift-client-1.0.jar "$(ifconfig | grep -oE '\b([0-9]{1,3}\.){3}[0-9]{1,3}\b' | head -1 | sed 's/.\{4\}$//').0.2"